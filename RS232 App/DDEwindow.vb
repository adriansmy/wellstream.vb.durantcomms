Option Strict On
Option Explicit On 
Option Compare Binary

' Author:   Mark Hurd
'
' Contact:  mailto:markhurd@ozemail.com.au
'
' Purpose:  To determine that DDE can be done in VB.NET.
'
' Description:
'
' A raw DDE process is described here:
'
'Beta 1:       ms-help://VSCC/MSDNVS/ipc/hh/winbase/dde_64dh.htm
'Beta 2: ms-help://MS.VSCC/MS.MSDNVS/ipc/hh/winbase/dde_64dh.htm
'
' It seems quite straightforward :-)
' 
' Note that this is *not* how I'd produce a 'final' version!
' However, if you're only implementing DDEExecute to receive
' command strings from Explorer, it would probably do.
'
' NB The code has evolved from Beta 1, so there may be some quirks.
'    To work around a repeatable IDE crash, I removed all Imports statements.
'    There are some quirks due to that. (I have since added some Imports back.)
'
' Please send me feedback. There is some good and bad code here.
''
' �2002 Mark Hurd. I put this in the Public Domain, however, if you use this
' source code for any purpose, send me an email and please forward me any
' updates or complete VB.NET DDE implementations based upon the ideas here.

Imports System.Runtime.InteropServices.Marshal

Namespace VBDDE

    Friend Class DDEwindow
        Inherits System.Windows.Forms.Form

        Public Shared Sub Main()
            System.Windows.Forms.Application.Run(New DDEwindow())
        End Sub

        Public Sub New()

            ' Create the form object
            MyBase.New()

            ' Look for DDE messages
            DDEmessages = New DDEmessageFilter(Handle)
        End Sub

#Region "Logging"
        Private ReadOnly NewLine As String = System.Environment.NewLine

        Private FormText As New System.Text.StringBuilder("Log:" & NewLine, 16384)

        Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)

            MyBase.OnPaint(e)

            With e.Graphics

                '.Clear(SystemColors.Window)
                .DrawString(FormText.ToString(), Font, System.Drawing.SystemBrushes.WindowText, 0!, 0!)
                .Flush()

            End With

        End Sub

        ' Instead of CStr(IIf(...))
        Private Overloads Function IIf(ByVal Condition As Boolean, ByVal TrueValue As String, ByVal FalseValue As String) As String
            If Condition Then
                Return TrueValue
            Else
                Return FalseValue
            End If
        End Function

        Private Sub Log(ByVal Text As String)
            FormText.Append(Text)
            Invalidate([Region], True)
        End Sub
#End Region

#Region "Message Loop and Filter"
        Private WithEvents DDEmessages As DDEmessageFilter

        Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
            If Not DDEmessages Is Nothing AndAlso DDEmessages.PreFilterMessage(m) Then
                ' Handled
            Else
                MyBase.WndProc(m)
            End If
        End Sub
#End Region

#Region "DDE (Message Received) Events"

        Private Sub DDEmessages_Initiate(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String) Handles DDEmessages.Initiate

            Log("Init(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ")" & NewLine)

            If (App.Length = 0 OrElse App.ToUpper() = "DDE.NET") AndAlso (Topic.Length = 0 OrElse Topic.ToLower() = "system") Then
                SendInitACK(hWnd, "DDE.NET", "System")
            End If
        End Sub

        Private Sub DDEmessages_InitACK(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String) Handles DDEmessages.InitACK

            Log("InitACK(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ") " & NewLine)

            If App.ToUpper = "PROGMAN" And Topic.ToUpper = "PROGMAN" Then
                PROGMANhWnd = hWnd
            End If
        End Sub

        Private Sub DDEmessages_Terminate(ByVal hWnd As System.IntPtr) Handles DDEmessages.Terminate

            Log("Terminate " & hWnd.ToInt32.ToString("x").ToUpper() & NewLine)

            PostTerminate(hWnd)

        End Sub

        Private Sub DDEmessages_Advise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal TransportAdvice As DDEADVISE) Handles DDEmessages.Advise

            Log("Advise " & hWnd.ToInt32.ToString("x").ToUpper() & ", Item: " & Item & NewLine _
                & "    " & IIf(TransportAdvice.fAckReq, "AckReq ", "       ") _
                & IIf(TransportAdvice.fDeferUpd, "DeferUpd ", "         ") _
                & TransportAdvice.cfFormat.Name _
                & NewLine)

            If TransportAdvice.fAckReq Then
                PostACK(hWnd, Item, DDEmessageFilter.ACKTYPES.OK, 0)
            End If
        End Sub

        Private Sub DDEmessages_Data(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal DataInfo As DDEDATA, ByVal DataPtr As System.IntPtr) Handles DDEmessages.Data

            Dim result As DDEmessageFilter.ACKTYPES = DDEmessageFilter.ACKTYPES.OK

            Log("Data " & hWnd.ToInt32.ToString("x").ToUpper() & ", Item: " & Item & NewLine _
                & "    " & IIf(DataInfo.fAckReq, "AckReq ", "       ") _
                & IIf(DataInfo.fResponse, "Response ", "         ") _
                & IIf(DataInfo.fRelease, "Release ", "         ") _
                & DataInfo.cfFormat.Name _
                & NewLine)

            Select Case DataInfo.cfFormat.Name
                Case System.Windows.Forms.DataFormats.UnicodeText
                    ' Use/assume unicode
                    Log(PtrToStringUni(DataPtr) & NewLine)
                Case System.Windows.Forms.DataFormats.Text
                    ' Use/assume ansi
                    Log(PtrToStringAnsi(DataPtr) & NewLine)
                Case Else
                    ' Unhandled data
                    result = DDEmessageFilter.ACKTYPES.NACK
            End Select

            If DataInfo.fAckReq Then
                PostACK(hWnd, Item, result, 0)
            End If
            'If DataInfo.fRelease Then
            '    ' Handled in caller
            'End If
            If DataInfo.fResponse Then
                DataReceived = True
            End If
        End Sub

        Private Sub DDEmessages_Execute(ByVal hWnd As System.IntPtr, ByVal Command As String, ByVal hCommand As System.IntPtr) Handles DDEmessages.Execute

            Log("Execute " & hWnd.ToInt32.ToString("x").ToUpper() & ": " & Command & NewLine)

            ' Always ACK:
            PostExecACK(hWnd, hCommand, DDEmessageFilter.ACKTYPES.OK, 0)
        End Sub

        Private Sub DDEmessages_Poke(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal PokeInfo As DDEPOKE, ByVal PokePtr As System.IntPtr) Handles DDEmessages.Poke

            Log("Poke " & hWnd.ToInt32.ToString("x").ToUpper() & " Item: " & Item & NewLine _
                & "    " & "       " _
                & IIf(PokeInfo.fRelease, "Release ", "         ") _
                & PokeInfo.cfFormat.Name _
                & NewLine)
        End Sub

        Private Sub DDEmessages_Request(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As System.Windows.Forms.DataFormats.Format) Handles DDEmessages.Request

            Log("Request " & hWnd.ToInt32.ToString("x").ToUpper() & " Item: " & Item & NewLine _
                & "    " & "       " _
                & Format.Name _
                & NewLine)
        End Sub

        Private Sub DDEmessages_UnAdvise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As System.Windows.Forms.DataFormats.Format) Handles DDEmessages.UnAdvise

            Log("UnAdvise " & hWnd.ToInt32.ToString("x").ToUpper() & " Item: " & Item & NewLine _
                & "    " & "       " _
                & Format.Name _
                & NewLine)
        End Sub

        Private Sub DDEmessages_ACK(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Response As DDEmessageFilter.ACKTYPES, ByVal AppReturnCode As Byte) Handles DDEmessages.ACK

            Log("ACK from " & hWnd.ToInt32.ToString("x").ToUpper() & ", Item: " & Item & NewLine _
                & "    " & " RetCode: " & AppReturnCode.ToString _
                & Response.ToString _
                & NewLine)

        End Sub
#End Region

#Region "Responses"

        Private Sub SendInitACK(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)

            Dim AppAtom As Short
            Dim TopicAtom As Short

            Try
                AppAtom = GlobalAddAtom(App)
                TopicAtom = GlobalAddAtom(Topic)
                SendMessage(hWnd, WM_DDE.ACK, Handle, MakeInt(AppAtom, TopicAtom))
                Log("   Init ACK sent(" & App & "," & Topic & ")" & NewLine)
            Finally
                If AppAtom <> 0 Then GlobalDeleteAtom(AppAtom)
                If TopicAtom <> 0 Then GlobalDeleteAtom(TopicAtom)
            End Try
        End Sub

        Private Sub PostTerminate(ByVal hWnd As System.IntPtr)

            Try
                PostMessage(hWnd, WM_DDE.TERMINATE, Handle, 0)

                Log("   Terminate Sent)" & NewLine)
            Finally
            End Try
        End Sub

        Private Sub PostACK(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Response As DDEmessageFilter.ACKTYPES, ByVal AppReturnCode As Byte)

            Dim AckData As DDEACK
            Dim ItemAtom As Short
            Dim hAckData As System.IntPtr
            Dim AckDataPtr As System.IntPtr
            Dim PostResult As Integer = 1

            Try
                ItemAtom = GlobalAddAtom(Item)
                AckData.bAppReturnCode = AppReturnCode
                Select Case Response
                    Case DDEmessageFilter.ACKTYPES.OK
                        AckData.fAck = True
                    Case DDEmessageFilter.ACKTYPES.Busy
                        AckData.fBusy = True
                    Case Else
                        'AckData.fAck = False
                        'AckData.fBusy = False
                End Select

                hAckData = AllocHGlobal(SizeOf(AckData))
                AckDataPtr = GlobalLock(hAckData)
                StructureToPtr(AckData, AckDataPtr, False)

                PostResult = PostMessage(hWnd, WM_DDE.ACK, Handle, PackDDElParam(WM_DDE.ACK, hAckData, ItemAtom))

                Log("   ACK sent(" & IIf(AckData.fAck, "ACK", "NACK") & ", " & AckData.bAppReturnCode & ")" & NewLine)

            Finally
                If ItemAtom <> 0 Then GlobalDeleteAtom(ItemAtom)
                With System.IntPtr.Zero
                    If Not .Equals(hAckData) Then
                        If Not .Equals(AckDataPtr) Then GlobalUnlock(hAckData)
                        If PostResult <> 0 Then FreeHGlobal(hAckData)
                    End If
                End With
            End Try
        End Sub

        Private Sub PostExecACK(ByVal hWnd As System.IntPtr, ByVal hCommand As System.IntPtr, ByVal Response As DDEmessageFilter.ACKTYPES, ByVal AppReturnCode As Byte)

            Dim AckData As DDEACK
            Dim hAckData As System.IntPtr
            Dim AckDataPtr As System.IntPtr
            Dim PostResult As Integer = 1

            Try
                AckData.bAppReturnCode = AppReturnCode
                Select Case Response
                    Case DDEmessageFilter.ACKTYPES.OK
                        AckData.fAck = True
                    Case DDEmessageFilter.ACKTYPES.Busy
                        AckData.fBusy = True
                    Case Else
                        'AckData.fAck = False
                        'AckData.fBusy = False
                End Select

                hAckData = AllocHGlobal(SizeOf(AckData))
                AckDataPtr = GlobalLock(hAckData)
                StructureToPtr(AckData, AckDataPtr, False)

                PostResult = PostMessage(hWnd, WM_DDE.ACK, Handle, PackDDElParam(WM_DDE.ACK, hAckData, hCommand))

                Log("   ACK sent(" & IIf(AckData.fAck, "ACK", "NACK") & ", " & AckData.bAppReturnCode & ")" & NewLine)
            Finally
                With System.IntPtr.Zero
                    If Not .Equals(hAckData) Then
                        If Not .Equals(AckDataPtr) Then GlobalUnlock(hAckData)
                        If PostResult <> 0 Then FreeHGlobal(hAckData)
                    End If
                End With
            End Try
        End Sub
#End Region

#Region "Extra code to request PROGMAN|PROGMAN!Groups"
        ' There are also changes in the InitACK abd Data events.
        Private PROGMANhWnd As System.IntPtr
        Private DataReceived As Boolean

        Private Sub DDEwindow_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.DoubleClick
            SendInitiate(BROADCAST, "PROGMAN", "PROGMAN")
            ' No need to wait, as Initiates are done using sendmessage
            If System.IntPtr.Zero.Equals(PROGMANhWnd) Then Exit Sub
            PostRequest(PROGMANhWnd, "Groups")
            WaitForData()
            PostTerminate(PROGMANhWnd)
        End Sub
        Private Sub SendInitiate(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)

            Dim AppAtom As Short
            Dim TopicAtom As Short

            Try
                AppAtom = GlobalAddAtom(App)
                TopicAtom = GlobalAddAtom(Topic)

                Log("   Init send(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ")" & NewLine)

                SendMessage(hWnd, WM_DDE.INITIATE, Handle, MakeInt(AppAtom, TopicAtom))

                Log("   Init done(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & App & "," & Topic & ")" & NewLine)
            Finally
                If AppAtom <> 0 Then GlobalDeleteAtom(AppAtom)
                If TopicAtom <> 0 Then GlobalDeleteAtom(TopicAtom)
            End Try
        End Sub
        Private Sub PostRequest(ByVal hWnd As System.IntPtr, ByVal Item As String)

            Dim ItemAtom As Short
            Dim FormatID As Short

            Try
                ItemAtom = GlobalAddAtom(Item)

                FormatID = CShort(System.Windows.Forms.DataFormats.GetFormat(System.Windows.Forms.DataFormats.Text).Id)

                DataReceived = False

                If PostMessage(hWnd, WM_DDE.REQUEST, Handle, PackDDElParam(WM_DDE.REQUEST, FormatID, ItemAtom)) = 0 Then
                    ' Clear the Item atom so we don't free it
                    ItemAtom = 0
                End If

                Log("   Request sent(" & hWnd.ToInt32.ToString("x").ToUpper() & ", " & FormatID & "," & Item & ")" & NewLine)
            Finally
                If ItemAtom <> 0 Then GlobalDeleteAtom(ItemAtom)
            End Try
        End Sub
        Private Sub WaitForData()
            Do
                If DataReceived Then Exit Sub
                System.Threading.Thread.Sleep(55)
                System.Windows.Forms.Application.DoEvents()
            Loop
        End Sub
#End Region

    End Class
End Namespace

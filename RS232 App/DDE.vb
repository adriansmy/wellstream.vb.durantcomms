Option Strict On
Option Explicit On 
Option Compare Binary

' Author:   Mark Hurd
'
' Contact:  mailto:markhurd@ozemail.com.au
'
' Purpose:  Details for DDE in VB.NET.
'
' Description:
'
' See DDEwindow.vb, and the links therein for more info.
'
' �2002 Mark Hurd. I put this in the Public Domain, however, if you use this
' source code for any purpose, send me an email and please forward me any
' updates or complete VB.NET DDE implementations based upon the ideas here.

Imports System.Runtime.InteropServices.LayoutKind
Imports System.Runtime.InteropServices.Marshal
Imports System.Windows.Forms.DataFormats

Namespace VBDDE

    Friend Module vbDDE

#Region "Windows Messages"
        ' These were not needed in Beta1 as they were present in Microsoft.Win32.Interop(.win)! 
        ' Order corresponds to dde.h - don't change!
        Friend Enum WM_DDE As Integer
            FIRST = &H3E0
            INITIATE = FIRST
            TERMINATE
            ADVISE
            UNADVISE
            ACK
            DATA
            REQUEST
            POKE
            EXECUTE
            LAST = EXECUTE

        End Enum

        Public Function IsDDEMsg(ByVal Msg As Integer) As Boolean
            'Return System.Enum.IsDefined(GetType(WM_DDE), Msg)
            Return (Msg And Not &HF) = WM_DDE.FIRST
        End Function
#End Region

#Region "DDE Communications Structures"
        'typedef struct { 
        '    unsigned short reserved:12, 'MEH adjusted from 14 to compare to DDEDATA
        '        reserved:1, 
        '        reserved:1, 
        '        fDeferUpd:1, 
        '        fAckReq:1; 
        '    short cfFormat; 
        '} DDEADVISE;
        'typedef struct { 
        '    unsigned short unused:12, 
        '        fResponse:1, 
        '        fRelease:1, 
        '        reserved:1, 
        '        fAckReq:1; 
        '    short cfFormat; 
        '    BYTE  Value[]; 
        '} DDEDATA; 

        <System.Runtime.InteropServices.StructLayout(Sequential)> _
        Private Structure DDEACKPREFIX
            Private Flags As Short 'PREFIXFlags

            <System.Flags()> Private Enum PREFIXFlags As Short
                AckCodeMask = &HFFS
                Response = &H1000S
                Release = &H2000S
                DeferUpd = &H4000S ' ACK:Busy
                AckReq = &H8000S  ' ACK:Ack
            End Enum
            Private Property fFlag(ByVal prefixFlag As PREFIXFlags) As Boolean
                Get
                    Return (Flags And prefixFlag) <> 0S
                End Get
                Set(ByVal Value As Boolean)
                    If Value Then
                        Flags = (Flags Or prefixFlag)
                    Else
                        Flags = (Flags And Not prefixFlag)
                    End If
                End Set
            End Property

            Public Property fResponse() As Boolean
                Get
                    Return (fFlag(PREFIXFlags.Response))
                End Get
                Set(ByVal Value As Boolean)
                    fFlag(PREFIXFlags.Response) = Value
                End Set
            End Property
            Public Property fRelease() As Boolean
                Get
                    Return fFlag(PREFIXFlags.Release)
                End Get
                Set(ByVal Value As Boolean)
                    fFlag(PREFIXFlags.Release) = Value
                End Set
            End Property
            Public Property fDeferUpd() As Boolean
                Get
                    Return fFlag(PREFIXFlags.DeferUpd)
                End Get
                Set(ByVal Value As Boolean)
                    fFlag(PREFIXFlags.DeferUpd) = Value
                End Set
            End Property
            Public Property fAckReq() As Boolean
                Get
                    Return fFlag(PREFIXFlags.AckReq)
                End Get
                Set(ByVal Value As Boolean)
                    fFlag(PREFIXFlags.AckReq) = Value
                End Set
            End Property

            Public Property bAppReturnCode() As Byte

                Get
                    Return CByte(Flags And PREFIXFlags.AckCodeMask)
                End Get
                Set(ByVal Value As Byte)
                    Flags = (CShort(Value) And PREFIXFlags.AckCodeMask) Or (Flags And Not PREFIXFlags.AckCodeMask)
                End Set
            End Property
        End Structure

        <System.Runtime.InteropServices.StructLayout(Sequential)> _
        Public Structure DDEACK
            Private Prefix As DDEACKPREFIX
            Public Property fBusy() As Boolean
                Get
                    Return Prefix.fDeferUpd
                End Get
                Set(ByVal Value As Boolean)
                    Prefix.fDeferUpd = Value
                End Set
            End Property
            Public Property fAck() As Boolean
                Get
                    Return Prefix.fAckReq
                End Get
                Set(ByVal Value As Boolean)
                    Prefix.fAckReq = Value
                End Set
            End Property
            Public Property bAppReturnCode() As Byte
                Get
                    Return Prefix.bAppReturnCode
                End Get
                Set(ByVal Value As Byte)
                    Prefix.bAppReturnCode = Value
                End Set
            End Property
        End Structure

        <System.Runtime.InteropServices.StructLayout(Sequential)> _
        Private Structure DDEDATAPREFIX
            Public Flags As DDEACKPREFIX
            Private Format As Short
            Public Property cfFormat() As Format
                Get
                    Return GetFormat(Format)
                End Get
                Set(ByVal Value As Format)
                    Format = CShort(Value.Id)
                End Set
            End Property
        End Structure

        <System.Runtime.InteropServices.StructLayout(Sequential)> _
        Public Structure DDEADVISE
            Private Prefix As DDEDATAPREFIX
            Public Property fDeferUpd() As Boolean
                Get
                    Return Prefix.Flags.fDeferUpd
                End Get
                Set(ByVal Value As Boolean)
                    Prefix.Flags.fDeferUpd = Value
                End Set
            End Property
            Public Property fAckReq() As Boolean
                Get
                    Return Prefix.Flags.fAckReq
                End Get
                Set(ByVal Value As Boolean)
                    Prefix.Flags.fAckReq = Value
                End Set
            End Property
            Public Property cfFormat() As Format
                Get
                    Return Prefix.cfFormat
                End Get
                Set(ByVal Value As Format)
                    Prefix.cfFormat = Value
                End Set
            End Property
        End Structure

        <System.Runtime.InteropServices.StructLayout(Sequential)> _
        Public Structure DDEDATA
            Private Prefix As DDEDATAPREFIX
            Public Property fResponse() As Boolean
                Get
                    Return Prefix.Flags.fResponse
                End Get
                Set(ByVal Value As Boolean)
                    Prefix.Flags.fResponse = Value
                End Set
            End Property
            Public Property fRelease() As Boolean
                Get
                    Return Prefix.Flags.fRelease
                End Get
                Set(ByVal Value As Boolean)
                    Prefix.Flags.fRelease = Value
                End Set
            End Property
            Public Property fAckReq() As Boolean
                Get
                    Return Prefix.Flags.fAckReq
                End Get
                Set(ByVal Value As Boolean)
                    Prefix.Flags.fAckReq = Value
                End Set
            End Property
            Public Property cfFormat() As Format
                Get
                    Return Prefix.cfFormat
                End Get
                Set(ByVal Value As Format)
                    Prefix.cfFormat = Value
                End Set
            End Property
        End Structure

        <System.Runtime.InteropServices.StructLayout(Sequential)> _
        Public Structure DDEPOKE
            Private Prefix As DDEDATAPREFIX
            Public Property fRelease() As Boolean
                Get
                    Return Prefix.Flags.fRelease
                End Get
                Set(ByVal Value As Boolean)
                    Prefix.Flags.fRelease = Value
                End Set
            End Property
            Public Property cfFormat() As Format
                Get
                    Return Prefix.cfFormat
                End Get
                Set(ByVal Value As Format)
                    Prefix.cfFormat = Value
                End Set
            End Property
        End Structure
#End Region

#Region "API Declarations"
        'Friend Declare Function GlobalLock Lib "Kernel32" (ByVal hMem As Integer) As System.IntPtr
        'Friend Declare Function GlobalUnlock Lib "Kernel32" (ByVal hMem As Integer) As System.IntPtr
        Friend Declare Function GlobalLock Lib "Kernel32" (ByVal hMem As System.IntPtr) As System.IntPtr
        Friend Declare Function GlobalUnlock Lib "Kernel32" (ByVal hMem As System.IntPtr) As System.IntPtr

        ' "For Posted DDE messages"...
        'Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
        'Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As Integer, ByRef LowWord As System.IntPtr, ByRef HighWord As Short) As Integer
        Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As System.IntPtr, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
        Friend Declare Function UnpackDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As System.IntPtr, ByRef LowWord As System.IntPtr, ByRef HighWord As Short) As Integer

#If False Then
        ' As defined in API documentation
        'Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
        Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As System.IntPtr, ByRef HighWord As Short) As Integer
        'Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As Short, ByRef HighWord As System.IntPtr) As Integer
        Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByRef LowWord As System.IntPtr, ByRef HighWord As System.IntPtr) As Integer
        'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As System.IntPtr, ByVal msgIn As Integer, ByVal msgOut As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
        'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As Integer, ByVal msgIn As Integer, ByVal msgOut As Integer, ByRef LowWord As Short, ByRef HighWord As Short) As Integer
#Else
        ' As makes sense
        Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As Short, ByVal HighWord As Short) As Integer
        Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As System.IntPtr, ByVal HighWord As Short) As Integer
        'Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As Short, ByVal HighWord As System.IntPtr) As Integer
        Friend Declare Function PackDDElParam Lib "User32" (ByVal msg As Integer, ByVal LowWord As System.IntPtr, ByVal HighWord As System.IntPtr) As Integer
        'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As System.IntPtr, ByVal msgIn As Integer, ByVal msgOut As Integer, ByVal LowWord As Short, ByVal HighWord As Short) As Integer
        'Friend Declare Function ReuseDDElParam Lib "User32" (ByVal lParam As Integer, ByVal msgIn As Integer, ByVal msgOut As Integer, ByVal LowWord As Short, ByVal HighWord As Short) As Integer
#End If

        Friend Declare Function FreeDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As System.IntPtr) As Integer
        'Friend Declare Function FreeDDElParam Lib "User32" (ByVal msg As Integer, ByVal lParam As Integer) As Integer

        Public ReadOnly BROADCAST As System.IntPtr = New System.IntPtr(-1)  ' Used for hWnd parameter below

        Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As System.IntPtr, ByVal lParam As Integer) As Integer
        Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As System.IntPtr, ByVal lParam As Integer) As Integer
        'Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer
        'Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As System.IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer
        'Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
        'Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As Integer
        'Friend Overloads Declare Ansi Function SendMessage Lib "User32" Alias "SendMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer
        'Friend Overloads Declare Ansi Function PostMessage Lib "User32" Alias "PostMessageA" (ByVal hWnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, <MarshalAs(UnmanagedType.BStr)> ByVal lParam As String) As Integer

        Friend Declare Auto Function InSendMessage Lib "User32" Alias "InSendMessage" () As Boolean

        Friend Declare Auto Function IsWindowUnicode Lib "User32" Alias "IsWindowUnicode" (ByVal hWnd As System.IntPtr) As Boolean

        Friend Declare Ansi Function GlobalAddAtom Lib "Kernel32" Alias "GlobalAddAtomA" (ByVal Buffer As String) As Short
        Friend Declare Ansi Function GlobalGetAtomName Lib "Kernel32" Alias "GlobalGetAtomNameA" (ByVal Atom As Short, ByVal Buffer As String, ByVal BufferLen As Integer) As Integer
        Friend Declare Auto Function GlobalDeleteAtom Lib "Kernel32" Alias "GlobalDeleteAtom" (ByVal Atom As Short) As Short

#End Region

    End Module

    Friend Class DDEmessageFilter
        Implements System.Windows.Forms.IMessageFilter

        Public hWnd As System.IntPtr

        Public Enum ACKTYPES
            OK
            Busy
            NACK
        End Enum

        Public Event Initiate(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)
        Public Event InitACK(ByVal hWnd As System.IntPtr, ByVal App As String, ByVal Topic As String)
        Public Event Terminate(ByVal hWnd As System.IntPtr)
        Public Event Advise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal TransportAdvice As DDEADVISE)
        Public Event UnAdvise(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As Format)
        Public Event ACK(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Response As ACKTYPES, ByVal AppReturnCode As Byte)
        Public Event Data(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal DataInfo As DDEDATA, ByVal DataPtr As System.IntPtr)
        Public Event Request(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal Format As Format)
        Public Event Poke(ByVal hWnd As System.IntPtr, ByVal Item As String, ByVal PokeInfo As DDEPOKE, ByVal PokePtr As System.IntPtr)
        Public Event Execute(ByVal hWnd As System.IntPtr, ByVal Command As String, ByVal hCommand As System.IntPtr)

        Private Function GetAtomString(ByVal Atom As Short) As String

            If Atom = 0 Then
                Return Nothing
            End If

            Dim BufferLen As Integer
            Dim Buffer As String

            BufferLen = 514
            Buffer = New String(Microsoft.VisualBasic.ChrW(0), BufferLen)
            BufferLen = GlobalGetAtomName(Atom, Buffer, BufferLen)
            If BufferLen = 0 Then
                System.Diagnostics.Trace.WriteLine("GetAtomString failed with DLL error number " & Microsoft.VisualBasic.Err.LastDllError())
                Return Nothing
            Else
                Return Buffer.Substring(0, BufferLen)
            End If

        End Function

        Protected Overridable Function OnInitiate(ByRef m As System.Windows.Forms.Message) As Boolean
            ' wparam is the client (window) handle
            ' lparam.low is atom of app
            ' lparam.high is atom of topic

            Dim AppAtom As Short
            Dim App As String
            Dim TopicAtom As Short
            Dim Topic As String

            UnpackDDElParam(m.Msg, m.LParam, AppAtom, TopicAtom)

            Topic = GetAtomString(TopicAtom)

            App = GetAtomString(AppAtom)

            RaiseEvent Initiate(m.WParam, App, Topic)

            FreeDDElParam(m.Msg, m.LParam)
            Return True
        End Function

        Protected Overridable Function OnTerminate(ByRef m As System.Windows.Forms.Message) As Boolean


            RaiseEvent Terminate(m.WParam)

            FreeDDElParam(m.Msg, m.LParam)
            Return True
        End Function

        Protected Overridable Function OnAdvise(ByRef m As System.Windows.Forms.Message) As Boolean
            ' wparam is the server (window) handle
            ' lparam.low is handle to DDEADVISE structure
            ' lparam.high is atom of item name

            Dim TransportAdvice As DDEADVISE
            Dim ItemAtom As Short
            Dim Item As String
            Dim hMem As System.IntPtr

            UnpackDDElParam(m.Msg, m.LParam, hMem, ItemAtom)

            Item = GetAtomString(ItemAtom)

            TransportAdvice = CType(PtrToStructure(GlobalLock(hMem), TransportAdvice.GetType()), DDEADVISE)

            RaiseEvent Advise(m.WParam, Item, TransportAdvice)

            GlobalUnlock(hMem)

            FreeDDElParam(m.Msg, m.LParam)
            Return True
        End Function

        Protected Overridable Function OnUnAdvise(ByRef m As System.Windows.Forms.Message) As Boolean
            Dim Format As Format
            Dim FormatID As Short
            Dim ItemAtom As Short
            Dim Item As String

            UnpackDDElParam(m.Msg, m.LParam, FormatID, ItemAtom)
            Format = GetFormat(FormatID)
            Item = GetAtomString(ItemAtom)
            RaiseEvent UnAdvise(m.WParam, Item, Format)

            FreeDDElParam(m.Msg, m.LParam)
            Return True
        End Function

        Protected Overridable Function OnAck(ByRef m As System.Windows.Forms.Message) As Boolean

            ' Need to know what has been sent!
            ' Specifically:
            '  If ACK of Initiate, it is two atoms (SENDMSG)
            If InSendMessage Then
                Dim AppAtom As Short
                Dim App As String
                Dim TopicAtom As Short
                Dim Topic As String

                SplitInt(m.LParam.ToInt32, AppAtom, TopicAtom)

                Topic = GetAtomString(TopicAtom)
                App = GetAtomString(AppAtom)
                RaiseEvent InitACK(m.WParam, App, Topic)
            Else
                '  If ACK of Execute, it is DDEACK and command string global object (POST)
                '  Otherwise, it is DDEACK and an atom (POST)
                Dim ItemAtom As Short
                Dim Item As String
                Dim AckData As DDEACK
                Dim hMem As System.IntPtr

                UnpackDDElParam(m.Msg, m.LParam, hMem, ItemAtom)

                Item = GetAtomString(ItemAtom)

                ' *** Assuming a command string ***

                AckData = CType(PtrToStructure(GlobalLock(hMem), AckData.GetType), DDEACK)

                Dim AckType As ACKTYPES = ACKTYPES.NACK

                If AckData.fAck Then
                    AckType = ACKTYPES.OK
                ElseIf AckData.fBusy Then
                    AckType = ACKTYPES.Busy
                End If

                RaiseEvent ACK(m.WParam, Item, AckType, AckData.bAppReturnCode)

                GlobalUnlock(hMem)
                GlobalDeleteAtom(ItemAtom)
                FreeDDElParam(m.Msg, m.LParam)
            End If

            Return True
        End Function

        Protected Overridable Function OnData(ByRef m As System.Windows.Forms.Message) As Boolean
            ' wparam is the server (window) handle
            ' lparam.low is handle to DDEDATA structure (or NULL)
            ' lparam.high is atom of item name
            Dim ItemAtom As Short
            Dim Item As String
            Dim hDDEData As System.IntPtr
            Dim pDDEData As System.IntPtr
            Dim DDEData As DDEData
            Dim FreeData As Boolean
            Try
                UnpackDDElParam(m.Msg, m.LParam, hDDEData, ItemAtom)

                Item = GetAtomString(ItemAtom)

                pDDEData = GlobalLock(hDDEData)

                DDEData = CType(PtrToStructure(pDDEData, DDEData.GetType()), DDEData)

                FreeData = DDEData.fRelease

                RaiseEvent Data(m.WParam, Item, DDEData, IncPtr(pDDEData, SizeOf(DDEData)))
            Finally
                GlobalUnlock(hDDEData)
                If FreeData Then
                    FreeHGlobal(hDDEData)
                End If

                FreeDDElParam(m.Msg, m.LParam)
            End Try
            Return True
        End Function

        Protected Overridable Function OnRequest(ByRef m As System.Windows.Forms.Message) As Boolean
            Dim Format As Format
            Dim FormatID As Short
            Dim ItemAtom As Short
            Dim Item As String
            UnpackDDElParam(m.Msg, m.LParam, FormatID, ItemAtom)
            Format = GetFormat(FormatID)
            Item = GetAtomString(ItemAtom)
            RaiseEvent Request(m.WParam, Item, Format)

            FreeDDElParam(m.Msg, m.LParam)
            Return True
        End Function

        Protected Overridable Function OnPoke(ByRef m As System.Windows.Forms.Message) As Boolean
            ' wparam is the client (window) handle
            ' lparam.low is handle to DDEPOKE structure (or NULL)
            ' lparam.high is atom of item name
            Dim DDEPoke As DDEPOKE
            Dim ItemAtom As Short
            Dim Item As String
            Dim hDDEPoke As System.IntPtr
            Dim pDDEPoke As System.IntPtr
            UnpackDDElParam(m.Msg, m.LParam, hDDEPoke, ItemAtom)
            Item = GetAtomString(ItemAtom)

            pDDEPoke = GlobalLock(hDDEPoke)
            PtrToStructure(pDDEPoke, DDEPoke)

            RaiseEvent Poke(m.WParam, Item, DDEPoke, IncPtr(pDDEPoke, SizeOf(DDEPoke)))

            GlobalUnlock(hDDEPoke)

            FreeDDElParam(m.Msg, m.LParam)
            Return True
        End Function

        Protected Overridable Function OnExecute(ByRef m As System.Windows.Forms.Message) As Boolean
            Dim Exec As String

            If IsWindowUnicode(m.HWnd) AndAlso IsWindowUnicode(m.WParam) Then
                ' Use/assume unicode
                Exec = PtrToStringUni(GlobalLock(m.LParam))
            Else
                ' Use/assume ansi
                Exec = PtrToStringAnsi(GlobalLock(m.LParam))
            End If

            RaiseEvent Execute(m.WParam, Exec, m.LParam)

            GlobalUnlock(m.LParam)

            FreeDDElParam(m.Msg, m.LParam)
            Return True
        End Function

        Public Function PreFilterMessage(ByRef m As System.Windows.Forms.Message) As Boolean _
            Implements System.Windows.Forms.IMessageFilter.PreFilterMessage

            Dim Broadcast As Boolean = (Broadcast.Equals(m.HWnd))

            If (Broadcast OrElse m.HWnd.Equals(hWnd)) AndAlso IsDDEMsg(m.Msg) Then

                Select Case m.Msg
                    Case WM_DDE.INITIATE
                        Return OnInitiate(m)
                    Case WM_DDE.TERMINATE
                        Return OnTerminate(m)
                    Case WM_DDE.ADVISE
                        Return OnAdvise(m)
                    Case WM_DDE.UNADVISE
                        Return OnUnAdvise(m)
                    Case WM_DDE.ACK
                        Return OnAck(m)
                    Case WM_DDE.DATA
                        Return OnData(m)
                    Case WM_DDE.REQUEST
                        Return OnRequest(m)
                    Case WM_DDE.POKE
                        Return OnPoke(m)
                    Case WM_DDE.EXECUTE
                        Return OnExecute(m)
                    Case Else
                        Return False
                End Select
                Return True
            Else
                Return False
            End If

        End Function

        Public Sub New(ByVal hWnd As System.IntPtr)
            MyBase.New()
            Me.hWnd = hWnd
        End Sub

    End Class

    Friend Module Utils
        <System.Runtime.InteropServices.StructLayout(Explicit)> _
        Private Structure CvtLong
            <System.Runtime.InteropServices.FieldOffset(0)> Public LongValue As Integer
            <System.Runtime.InteropServices.FieldOffset(0)> Public LoWord As Short
            <System.Runtime.InteropServices.FieldOffset(2)> Public HiWord As Short
        End Structure

        ' "MAKELONG"
        Public Function MakeInt(ByVal LoWord As Short, ByVal HiWord As Short) As Integer
            Dim Convert As CvtLong
            Convert.HiWord = HiWord
            Convert.LoWord = LoWord
            Return Convert.LongValue
        End Function

        Public Sub SplitInt(ByVal Value As Integer, ByRef LoWord As Short, ByRef HiWord As Short)
            Dim Convert As CvtLong
            Convert.LongValue = Value
            HiWord = Convert.HiWord
            LoWord = Convert.LoWord
        End Sub

        ' Only saves a character or two, but it looks cleaner
        Public Function IncPtr(ByVal Ptr As System.IntPtr, ByVal Offset As Integer) As System.IntPtr
            Return New System.IntPtr(Ptr.ToInt32 + Offset)
        End Function
    End Module

End Namespace
Public Class RS232Form
    Inherits System.Windows.Forms.Form

    '// Private members
    Private miComPort As Integer
    Friend WithEvents btnTx As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTx As System.Windows.Forms.TextBox
    Friend WithEvents txtRx As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Private WithEvents moRS232 As Rs232
    Friend WithEvents chkAddCR As System.Windows.Forms.CheckBox
    Public WithEvents IntouchValue As System.Windows.Forms.Label
    Private mlTicks As Long
    Private Delegate Sub CommEventUpdate(ByVal source As Rs232, ByVal mask As Rs232.EventMasks)

    Public CurrentChar As String
    Public Char1 As String
    Public Char2 As String
    Public Char3 As String
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Public StartedValue As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
    Private components As System.ComponentModel.IContainer


    'Required by the Windows Form Designer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnTx = New System.Windows.Forms.Button
        Me.txtTx = New System.Windows.Forms.TextBox
        Me.chkAddCR = New System.Windows.Forms.CheckBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtRx = New System.Windows.Forms.TextBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.IntouchValue = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'btnTx
        '
        Me.btnTx.Enabled = False
        Me.btnTx.Location = New System.Drawing.Point(216, 30)
        Me.btnTx.Name = "btnTx"
        Me.btnTx.Size = New System.Drawing.Size(60, 21)
        Me.btnTx.TabIndex = 7
        Me.btnTx.Text = "Tx"
        Me.ToolTip1.SetToolTip(Me.btnTx, "Sends data to COM Port")
        '
        'txtTx
        '
        Me.txtTx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTx.Location = New System.Drawing.Point(7, 30)
        Me.txtTx.Name = "txtTx"
        Me.txtTx.Size = New System.Drawing.Size(200, 21)
        Me.txtTx.TabIndex = 6
        Me.txtTx.Text = "?"
        Me.ToolTip1.SetToolTip(Me.txtTx, "Type data you want to send")
        '
        'chkAddCR
        '
        Me.chkAddCR.Location = New System.Drawing.Point(216, 57)
        Me.chkAddCR.Name = "chkAddCR"
        Me.chkAddCR.Size = New System.Drawing.Size(69, 15)
        Me.chkAddCR.TabIndex = 31
        Me.chkAddCR.Text = "Add CR"
        Me.ToolTip1.SetToolTip(Me.chkAddCR, "Add a CR at the end of Tx Message")
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(4, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 17)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Received Data"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(4, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 18)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Data to Tx"
        '
        'txtRx
        '
        Me.txtRx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRx.Location = New System.Drawing.Point(7, 95)
        Me.txtRx.Multiline = True
        Me.txtRx.Name = "txtRx"
        Me.txtRx.Size = New System.Drawing.Size(305, 39)
        Me.txtRx.TabIndex = 9
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(251, 144)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(61, 26)
        Me.btnExit.TabIndex = 14
        Me.btnExit.Text = "&Close"
        '
        'IntouchValue
        '
        Me.IntouchValue.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.IntouchValue.Location = New System.Drawing.Point(4, 150)
        Me.IntouchValue.Name = "IntouchValue"
        Me.IntouchValue.Size = New System.Drawing.Size(82, 15)
        Me.IntouchValue.TabIndex = 32
        Me.IntouchValue.Text = "Intouch Value"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'RS232Form
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(319, 172)
        Me.Controls.Add(Me.IntouchValue)
        Me.Controls.Add(Me.chkAddCR)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtRx)
        Me.Controls.Add(Me.txtTx)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnTx)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "RS232Form"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Wellstream Durant Comms"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region




    Private Sub btnOpenCom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        moRS232 = New Rs232()
        Try
            '// Setup parameters
            With moRS232
                .Port = 3
                .BaudRate = 1200
                .DataBit = 7
                .StopBit = Rs232.DataStopBit.StopBit_1
                .Parity = Rs232.DataParity.Parity_Even
                .Timeout = 30000
            End With
            '// Initializes port
            moRS232.Open()
            '// Set state of RTS / DTS
            'moRS232.Dtr = (chkDTR.CheckState = CheckState.Checked)
            'moRS232.Rts = (chkRTS.CheckState = CheckState.Checked)

            'If chkEvents.Checked Then moRS232.EnableEvents()
            moRS232.EnableEvents()
            'chkEvents.Enabled = True
        Catch Ex As Exception
            MessageBox.Show(Ex.Message, "Connection Error", MessageBoxButtons.OK)
        Finally
            'btnCloseCom.Enabled = moRS232.IsOpen
            'btnOpenCom.Enabled = Not moRS232.IsOpen
            btnTx.Enabled = moRS232.IsOpen
        End Try
    End Sub

    Private Sub AutoOpenComms()

        moRS232 = New Rs232()
        Try
            '// Setup parameters
            With moRS232
                .Port = 3
                .BaudRate = 1200
                .DataBit = 7
                .StopBit = Rs232.DataStopBit.StopBit_1
                .Parity = Rs232.DataParity.Parity_Even
                .Timeout = 30000
            End With
            '// Initializes port
            moRS232.Open()
            '// Set state of RTS / DTS
            'moRS232.Dtr = (chkDTR.CheckState = CheckState.Checked)
            'moRS232.Rts = (chkRTS.CheckState = CheckState.Checked)

            'If chkEvents.Checked Then moRS232.EnableEvents()
            moRS232.EnableEvents()
            'chkEvents.Enabled = True
        Catch Ex As Exception
            MessageBox.Show(Ex.Message, "Connection Error", MessageBoxButtons.OK)
        Finally
            'btnCloseCom.Enabled = moRS232.IsOpen
            'btnOpenCom.Enabled = Not moRS232.IsOpen
            btnTx.Enabled = moRS232.IsOpen
        End Try

    End Sub


    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTx.Click
        Dim sTx As String
        '----------------------
        '// Clear Tx/Rx Buffers
        moRS232.PurgeBuffer(Rs232.PurgeBuffers.TxClear Or Rs232.PurgeBuffers.RXClear)
        sTx = txtTx.Text
        If chkAddCR.Checked Then sTx += ControlChars.Cr
        moRS232.Write(sTx)
        'moRS232.Write(Chr(2) & Chr(2) & Chr(73) & Chr(48) & Chr(121) & Chr(3))
        '// Clears Rx textbox
        txtRx.Text = String.Empty
        txtRx.Refresh()
        'lbHex.Items.Clear()
        'If chkAutorx.Checked Then Button1_Click(Nothing, Nothing)
    End Sub

    Private Sub Form1_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If Not moRS232 Is Nothing Then
            '// Disables Events if active
            moRS232.DisableEvents()
            If moRS232.IsOpen Then moRS232.Close()
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub


    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Call AutoOpenComms()

        Call DDEModule.Register()

    End Sub


    Private Sub moRS232_CommEvent(ByVal source As Rs232, ByVal Mask As Rs232.EventMasks) Handles moRS232.CommEvent
        '===================================================
        '												�2003 www.codeworks.it All rights reserved
        '
        '	Description	:	Events raised when a comunication event occurs
        '	Created			:	15/07/03 - 15:13:46
        '	Author			:	Corrado Cavalli
        '
        '						*Parameters Info*
        '
        '	Notes				:	
        '===================================================
        Debug.Assert(Me.InvokeRequired = False)

        Dim iPnt As Int32, sBuf As String, TmpStr As String, Buffer() As Byte
        Debug.Assert(Me.InvokeRequired = False)
        If (Mask And Rs232.EventMasks.RxChar) > 0 Then
            'lbHex.Items.Add("Received data: " & source.InputStreamString)

            Buffer = source.InputStream
            CurrentChar = (String.Format("0x{0}", Buffer(iPnt).ToString("X")))

            If txtRx.Text = "" And CurrentChar = "0xD" Then
                Char1 = CurrentChar
                Exit Sub
            End If

            If txtRx.Text = "" And CurrentChar = "0xA" And Char1 = "0xD" Then
                Char2 = CurrentChar
                Exit Sub
            ElseIf txtRx.Text = "" And CurrentChar <> "0xA" And Char2 <> "0xA" And Char1 = "0xD" Then
                Char1 = ""
                Exit Sub
            End If

            If txtRx.Text = "" And source.InputStreamString = "C" And Char2 = "0xA" And Char1 = "0xD" Then
                txtRx.Text = txtRx.Text & source.InputStreamString
                Exit Sub
            ElseIf txtRx.Text = "" And source.InputStreamString <> "C" And Char2 = "0xA" And Char1 = "0xD" Then
                txtRx.Clear()
                Char1 = ""
                Char2 = ""
            End If

            If txtRx.Text <> "" And source.InputStreamString <> "C" And CurrentChar <> "0xD" And CurrentChar <> "0xA" Then
                txtRx.Text = txtRx.Text & source.InputStreamString
                Char1 = ""
                Char2 = ""
                Exit Sub
            End If

            If txtRx.TextLength = 11 And CurrentChar = "0xD" Then
                Char3 = CurrentChar
            End If

            TmpStr = txtRx.Text
            If txtRx.TextLength = 11 And CurrentChar = "0xA" And Char3 = "0xD" Then
                If TmpStr.StartsWith("CNT ") And IsNumeric(TmpStr.Substring(4)) Then
                    IntouchValue.Text = TmpStr.Substring(4)
                    IntouchValue1 = TmpStr.Substring(4)
                    If CurrentChar = "0xD" Then
                        Char1 = "0xD"
                    Else
                        Char1 = ""
                    End If
                    Char2 = ""
                    Char3 = ""
                End If
                Char3 = ""
                txtRx.Clear()
            ElseIf txtRx.Text <> "" And txtRx.TextLength <> 11 And (CurrentChar = "0xA" Or CurrentChar = "0xD") Then
                IntouchValue.Text = "Invalid"
                IntouchValue1 = "Invalid"
                If CurrentChar = "0xD" Then
                    Char1 = "0xD"
                Else
                    Char1 = ""
                End If
                Char2 = ""
                Char3 = ""
                txtRx.Clear()
            ElseIf txtRx.TextLength > 11 Then
                IntouchValue.Text = "Invalid"
                IntouchValue1 = "Invalid"
                If CurrentChar = "0xD" Then
                    Char1 = "0xD"
                Else
                    Char1 = ""
                End If
                Char2 = ""
                Char3 = ""
                txtRx.Clear()
            End If
        End If
    End Sub

#Region "UI update routine"
#End Region


    'Private Sub lbHex_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbHex.DoubleClick
    '    lbHex.Items.Clear()
    'End Sub

    Private Sub txtRx_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRx.DoubleClick
        txtRx.Text = String.Empty
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim sTx As String
        '----------------------
        '// Clear Tx/Rx Buffers
        moRS232.PurgeBuffer(Rs232.PurgeBuffers.TxClear Or Rs232.PurgeBuffers.RXClear)
        sTx = txtTx.Text
        If chkAddCR.Checked Then sTx += ControlChars.Cr
        moRS232.Write(sTx)
        'moRS232.Write(Chr(2) & Chr(2) & Chr(73) & Chr(48) & Chr(121) & Chr(3))
        '// Clears Rx textbox
        txtRx.Text = String.Empty
        txtRx.Refresh()
        'lbHex.Items.Clear()
        'If chkAutorx.Checked Then Button1_Click(Nothing, Nothing)
    End Sub
End Class

